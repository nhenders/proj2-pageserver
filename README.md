# Project 2: Page Server

Nick Henderson

nhenders@uoregon.edu / nick@nihenderson.com

Simple Flask web server to send contents of .html or .css files with http 
response, or serve 404 or 403 pages appropriately.
