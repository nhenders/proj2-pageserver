from flask import Flask, request, render_template, abort

app = Flask(__name__)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def file_forbidden(e):
    return render_template('403.html'), 403

@app.route('/<path:path>')
def serve_at_path(path):
    
    # detect illegal symbols and respond with 403
    if path.startswith(("~", "..", "/")):
        abort(403)
    
    # try to serve page at path, or respond with 404 if it doesn't exist
    try:
        return render_template(path), 200
    except TemplateNotFound:
        abort(404)

    return

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
